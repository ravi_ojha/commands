##Steps for running this

```
$ cd $GOPATH/src/
$ mkdir bitbucket.org/ -- (if this folder doesn't exist)
$ cd bitbucket.org
$ mkdir ravi_ojha
$ cd ravi_ojha
$ git clone https://ravi_ojha@bitbucket.org/ravi_ojha/commands.git
$ cd commands/
$ ./run.sh or (./run.sh -in=/absolute/path/to/file/input)
```

##Shortcut??
go to this [link](https://bitbucket.org/ravi_ojha/commands/src/558e634371255f916deaf376bf7ec2e4d523c88c/scripts/setup.sh?at=setup&fileviewer=file-view-default)
Copy the content of the file and paste it in new file (lets say in home directory) as setup.sh

```
$ chmod +x ~/setup.sh
$ ./setup.sh
$ cd $GOPATH/src/bitbucket.org/ravi_ojha/commands
$ ./run.sh or (./run.sh -in=/absolute/path/to/file/input)
```