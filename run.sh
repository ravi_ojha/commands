#!/usr/bin/env bash
set -ex
go install bitbucket.org/ravi_ojha/commands
exec commands "$@"