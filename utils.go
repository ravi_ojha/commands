package main

import (
	"fmt"
	"os"
	filepath "path/filepath"
	"strings"
)

func logf(t string, vals ...interface{}) {
	fmt.Printf(t, vals...)
}

func commandParser(cmd string, args string) {
	switch strings.ToLower(cmd) {
	case gCd:
		err := changeDirectory(args, wd)
		if err != nil {
			logf("Error: %v\n", err)
		}
	case gMkdir:
		err := makeDirectory(args)
		if err != nil {
			logf("Error: %v\n", err)
		}
	case gRm:
		err := remove(args)
		if err != nil {
			logf("Error: %v\n", err)
		}
	case gPwd:
		err := showCurrentWorkingDirector()
		if err != nil {
			logf("Error: %v\n", err)
		}
	case gClear:
		err := sessionClear(wd)
		if err != nil {
			logf("Error: %v\n", err)
		}
	default:
		logf("Error: Oops, we couldn't understand you, Please try again\n")
	}
}

func changeDirectory(args string, dir string) error {
	var path string
	// isAbs := filepath.IsAbs(args)
	//
	// if isAbs {
	// 	path = filepath.Join(args)
	// 	s, err := os.Stat(path)
	// 	if err == nil && s.IsDir() {
	// 		os.Chdir(path)
	// 	} else {
	// 		return err
	// 	}
	// } else {
	// 	path = filepath.Join(dir, args)
	// 	s, err := os.Stat(path)
	// 	if err == nil && s.IsDir() {
	// 		os.Chdir(path)
	// 	} else {
	// 		return err
	// 	}
	// }

	// Doing only with respect of working directory
	if args == "/" {
		path = filepath.Join(dir)
	} else {
		dir1, err := os.Getwd()
		if err != nil {
			return err
		}
		path = filepath.Join(dir1, args)
	}
	s, err := os.Stat(path)
	if err == nil && s.IsDir() {
		os.Chdir(path)
	} else {
		return err
	}
	logf("Success: %v\n", path)
	return nil
}

func makeDirectory(args string) error {
	dir, err := os.Getwd()
	if err == nil {
		path := filepath.Join(dir, args)
		err = os.Mkdir(path, 0777)
		if err != nil {
			return err
		}
	} else {
		return err
	}
	logf("Success: %v directory created\n", args)
	return nil
}

func remove(args string) error {
	dir, err := os.Getwd()
	info := ""
	if err == nil {
		path := filepath.Join(dir, args)
		fi, err := os.Stat(path)
		if err == nil {
			// if fi.IsDir() {
			// 	// return errors.New("Provided path is a directory")
			// 	y := false
			// 	fmt.Printf("Provided path is a directory, Do you want to continue? (y)")
			// 	_, err = fmt.Scanf("%v", &y)
			// 	if y {
			// 		err = os.RemoveAll(path)
			// 		if err != nil {
			// 			return err
			// 		}
			// 	}
			// } else {
			// 	err = os.Remove(path)
			// 	if err != nil {
			// 		return err
			// 	}
			// }

			// Removing directory even though you may need to provide -rf for deleting a directory
			if fi.IsDir() {
				info = "directory"
			} else {
				info = "file"
			}
			err = os.RemoveAll(path)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}
	logf("Success: %v %v removed\n", info, args)
	return nil
}

func showCurrentWorkingDirector() error {
	dir, err := os.Getwd()
	if err != nil {
		return err
	}
	logf("Success: %v\n", dir)
	return nil
}

func sessionClear(dir string) error {
	s, err := os.Stat(dir)
	if err == nil && s.IsDir() {
		os.Chdir(dir)
	}
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	return nil
}
