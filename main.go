package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var (
	gClear = "session"
	gCd    = "cd"
	gMkdir = "mkdir"
	gRm    = "rm"
	gPwd   = "pwd"
	gClose = "close"
)

var (
	in = flag.String("in", "", "Input command file")
)

var wd string

func usage() {
	fmt.Printf("Usage:  %s [options]\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	fmt.Println("<Starting your application...>")
	flag.Usage = usage
	flag.Parse()
	if flag.NFlag() == 0 {
		flag.Usage()
		fmt.Println("<No Input file is given, so type in manually>")
	}

	now := time.Now()
	dir := strconv.Itoa(int(now.Unix()))
	wd = filepath.Join("/tmp", dir)

	err := os.Mkdir(wd, 0777)
	if err != nil {
		logf("Something went wrong, Error: %v \n", err)
	} else {
		os.Chdir(wd)
	}

	if *in == "" {
		inputByCLI()
	} else {
		inputByFile(*in)
	}
}

func inputByCLI() {
	var cmd string
	var args string
	fmt.Printf("$ ")
	n, err := fmt.Scanf("%v %v\n", &cmd, &args)
	if err != nil && n == 2 {
		logf("Something went wrong, Error: %v \n", err)
	}
	for strings.ToLower(cmd) != gClose {
		commandParser(cmd, args)
		fmt.Printf("$ ")
		_, err = fmt.Scanf("%v %v\n", &cmd, &args)
	}
}

func inputByFile(inp string) {
	file, err := os.Open(inp)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		s := scanner.Text()
		fmt.Println(s)
		cmd := ""
		args := ""

		ss := strings.Split(s, " ")
		cmd = ss[0]
		if len(ss) > 1 {
			args = ss[1]
		}

		if strings.ToLower(cmd) != gClose {
			commandParser(cmd, args)
		}
	}
}
